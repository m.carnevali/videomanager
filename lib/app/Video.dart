import 'package:flutter/cupertino.dart';

class Video {
  final int id;
  final String videoURL;
  final String title;
  final String description;
  bool liked;

  Video(
      {@required this.id,
      @required this.videoURL,
      @required this.title,
      this.description,
      this.liked = false});

  factory Video.fromJson(Map<String, dynamic> json) {
    var video = new Video(
        id: json['id'],
        videoURL: json['videoURL'],
        title: json['title'],
        description: json['description'],
        liked: json['liked'] == 1 ? true : false);

    return video;
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {
      'id': this.id,
      'videoURL': this.videoURL,
      'title': this.title,
      'description': this.description,
      'liked': this.liked ? 1 : 0
    };

    return json;
  }
}
