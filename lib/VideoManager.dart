library VideoManager;

import 'package:flutter/material.dart';
import 'package:VideoManager/app/Video.dart';
import 'package:video_player/video_player.dart';

/// A Calculator.
class Calculator {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
}

class VideoManager extends StatefulWidget {
  //final Video myVideo;
  //const VideoManager({Key key, this.myVideo}) : super(key: key);
  @required
  final int id;
  @required
  final String videoURL;
  @required
  final String title;
  final String description;
  @required
  final bool liked;
  @required
  final Function addFavorite;
  @required
  final Function removeFavorite;

  const VideoManager(
      {Key key,
      this.id,
      this.videoURL,
      this.title,
      this.description = '',
      this.liked,
      this.addFavorite,
      this.removeFavorite})
      : super(key: key);

  @override
  _VideoManagerState createState() => _VideoManagerState();
}

class _VideoManagerState extends State<VideoManager> {
  VideoPlayerController _controller;
  Video myVideo;

  bool isLoad = false;

  //SOTTOTITOLI
  /*Future<ClosedCaptionFile> _loadCaptions() async {
    final String fileContents = await DefaultAssetBundle.of(context)
        .loadString('assets/bumble_bee_captions.srt');
    return SubRipCaptionFile(fileContents);
  }*/

  @override
  void initState() {
    super.initState();

    //init obj Video
    myVideo = new Video(
        id: widget.id,
        videoURL: widget.videoURL,
        title: widget.title,
        description: widget.description,
        liked: widget.liked);

    //_controller = VideoPlayerController.asset('assets/videos/20200602.mp4');
    _controller = VideoPlayerController.network(
      myVideo.videoURL,
      //'https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4',
      //closedCaptionFile: _loadCaptions(),
      videoPlayerOptions: VideoPlayerOptions(
          mixWithOthers:
              true), // pour pas bloquer les autres vidéos qu'ils sont déjà en exécution
    );

    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);

    _controller.initialize().then((_) => setState(() {
          isLoad = true;
        }));
    //_controller.play();
  }

  @override
  void dispose() {
    super.dispose();

    _controller.dispose(); //distruggere l'elemento controller
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2,
        color: Colors.blueGrey[50],
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                myVideo.title,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _controller.value.initialized
                  ? InkWell(
                      onTap: () {
                        _controller.value.isPlaying
                            ? _controller.pause()
                            : _controller.play();
                      },
                      onDoubleTap: () {
                        setState(() {
                          widget.liked
                              ? widget.removeFavorite()
                              : widget.addFavorite();
                        });
                      },
                      child: AspectRatio(
                        aspectRatio: 16 / 9,
                        child: isLoad
                            ? VideoPlayer(_controller)
                            : CircularProgressIndicator(),
                      ),
                    )
                  : Container(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      _controller.value.isPlaying
                          ? Icons.pause_circle_outline
                          : Icons.play_circle_outline,
                      size: 30,
                    ),
                    onPressed: () {
                      _controller.value.isPlaying
                          ? _controller.pause()
                          : _controller.play();
                    },
                  ),
                  IconButton(
                    onPressed: () {
                      setState(() {
                        widget.liked
                            ? widget.removeFavorite()
                            : widget.addFavorite();
                      });
                    },
                    icon: Icon(
                      widget.liked ? Icons.favorite : Icons.favorite_border,
                      size: 30,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
